import QtQuick 2.0
import QtQuick.Particles 2.0

Rectangle {
    id: root

    anchors.fill: parent
    color: "#3f3f3f"

    ParticleSystem {
        id: system

        anchors.fill: parent
    }
}
