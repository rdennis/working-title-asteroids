import QtQuick 2.0

Item {
    id: root

    anchors.centerIn: parent
    width: Math.max(parent.width, parent.height) * 4; height: width

    function explode() {
        sprite.start()
    }

    signal finished

    AnimatedSprite {
        id: sprite

        visible: running
        anchors.fill: parent
        running: false
        source: "../imgs/explosion.png"
        frameCount: 42
        frameWidth: 130
        frameHeight: 130
        frameSync: true
        loops: 1

        onCurrentFrameChanged: {
            if(currentFrame == frameCount - 1) {
                sprite.running = false
                sprite.currentFrame = 0
                root.finished()
            }
        }
    }
}
