import QtQuick 2.0
import "../collisions.js" as CollisionManager

Item {
    id: root

    width: 500
    height: width
    state: "PLAYING_GAME"

    property var asteroidComponent: Qt.createComponent("Asteroid.qml")
    property int maxAsteroids: 10

    function spawnAsteroid(size, p) {
        if(asteroidComponent.status === Component.Ready)
            var incubator = asteroidComponent.incubateObject(root,(size&&p?{size:size, x:p.x, y:p.y}:{}))
        else
            console.log("asteroidComponent not ready")
    }

    function destroyAsteroids() {
        var asteroids = CollisionManager.getEntitiesInGroup("asteroid")

        for (var asteroid in asteroids) {
            asteroids[asteroid].explode()
        }
    }

    Timer {
        id:collisionTimer
        interval: 10
        repeat: true
        onTriggered: CollisionManager.collideEntities()
    }

    // background StarField
    StarField {}

    focus: true
    Keys.onEscapePressed: Qt.quit()

    Keys.onPressed: {
        if(root.state === "GAME_OVER")
            return

        if(event.key === Qt.Key_W || event.key === Qt.Key_Up) {
            player.accelerate()
        } else if(event.key === Qt.Key_Space) {
            player.cannon.fire()
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

        acceptedButtons: Qt.AllButtons
        cursorShape: Qt.CrossCursor
    }

    Player {
        id: player

        height: 40
        width: 40

        onKilled: root.state = "GAME_OVER"
    }

    Timer {
        id: asteroidSpawner

        running: root.state === "PLAYING_GAME"
        repeat: true
        interval: 1000
        onTriggered: {
            if(CollisionManager.getAsteroidCount() < maxAsteroids) {
                spawnAsteroid()
            }
        }
    }

    Text {
        id: gameOverText
        font.pointSize: 20
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.fill: root
        text: "Game Over\nClick anywhere to play again"
    }

    states: [
        State {
            name: "PLAYING_GAME"

            PropertyChanges {
                target: gameOverText
                visible: false
            }
            PropertyChanges {
                target: player
                visible: true
                focalPoint: Qt.point(mouseArea.mouseX, mouseArea.mouseY)
                x: root.width / 2 - width / 2
                y: root.height / 2 - height / 2
            }
            PropertyChanges {
                target: mouseArea

                onClicked: {
                    if(mouse.button == Qt.LeftButton)
                        player.accelerate()
                    else
                        player.velocity = Qt.point(0,0)
                }
            }
            PropertyChanges {
                target: collisionTimer
                running: true
            }
            StateChangeScript {
                script: destroyAsteroids()
            }
        },
        State {
            name: "GAME_OVER"

            PropertyChanges {
                target: gameOverText
                visible: true
            }
            PropertyChanges {
                target: player
                visible: false
            }
            PropertyChanges {
                target: mouseArea

                onClicked: root.state = "PLAYING_GAME"
            }
            PropertyChanges {
                target: collisionTimer
                running: false
            }
        }
    ]
}
