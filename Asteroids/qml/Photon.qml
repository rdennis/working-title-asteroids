import QtQuick 2.0
import "../collisions.js" as CollisionManager

Entity {
    id: root

    entityClass: "Photon"

    focalLock: false
    screenWrap: false

    sprite: Rectangle {
        id: sprite

        width: 10; height: 3
        color: "white"
        border.color: "black"
    }

    onXChanged: {
        if(x < 0 || x > parent.width)
            root.destroy()
    }

    onYChanged: {
        if(y < 0 || y > parent.height)
            root.destroy()
    }

    onCollision: {
        CollisionManager.removeEntity(root)
        root.velocity = other.velocity
        sprite = null
        if(other.entityClass === "Asteroid" && other.size < 2)
            return
        explosion.explode()
    }

    Explosion {
        id: explosion

        onFinished: root.destroy()
    }
}
