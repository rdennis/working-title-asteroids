import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    id: root
    property alias fireRate: ship.fireRate
    property alias movementSpeed: ship.movementSpeed

    focus: true
    Keys.onEscapePressed: {Qt.quit()}

    Keys.onPressed: {
        if(event.key === Qt.Key_Up || event.key === Qt.Key_W) {
            ship.fireThrusters()
        }
    }

    Keys.onSpacePressed: { ship.fireCannon() }

    Rectangle {
        anchors.top: parent.top
        anchors.left: parent.left
        color: "black"

        property double r: ship.rotation
        Text {
            id: instructionText
            renderType: Text.NativeRendering
            color: "white"
            text: qsTr("Controls:\n   Thrusters: W or Up\n   Fire: LClick or Space")
        }
        Text {
            renderType: Text.NativeRendering
            id: debugText
            color: "white"
            text: qsTr("Rotation: " + parent.r)
            anchors.top: instructionText.bottom
        }
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        hoverEnabled: true
        onPressed: { ship.fireCannon() }
        onPressAndHold: { repeater.start() }
        onReleased: {
            if(repeater.running)
                repeater.stop()
        }

        Timer {
            id: repeater
            interval: ship.fireRate
            running: false
            repeat: true
            onTriggered: { ship.fireCannon() }
        }
    }

    ParticleSystem {
        id: particleSystem
        anchors.fill: parent

        ItemParticle {
            id: trailParticle
            groups: ["trail"]
            width: 5; height: 5
            delegate: Rectangle {
                width: trailParticle.width; height: trailParticle.height
                radius: width / 2
                color: "cyan"
            }
        }

        ItemParticle {
            id: photonParticle
            groups: ["photon"]
            width: 12; height: 2
            fade: false
            delegate: Rectangle {
                width: photonParticle.width; height: photonParticle.height
                radius: height / 2
                color: "white"
                // assign (no binging)
                rotation: getRotation()
                function getRotation() {
                    rotation = ship.rotation
                }
            }
        }
    }

    Image {
        id: ship

        Rectangle {
            id: box
            anchors.fill: parent
            color: "transparent"
            border.color: "red"
        }

        Rectangle {
            id: target
            color: "red"
            width: root.width; height: 1
            x: parent.width
            y: parent.height / 2
        }

        source: "../../imgs/ship.png"        
        // start in center of screen
        anchors.centerIn: parent
        // start facing up
        rotation: 270

        // point at which to aim
        property var focalPoint
        property double movementSpeed: 1
        property double maxSpeed: 10
        property double velocityX: 0
        property double velocityY: 0
        property int fireRate: 300
        property bool canFire: true
        function currentSpeed() {
            return (Math.abs(velocityX) + Math.abs(velocityY)) * movementSpeed
        }

        onXChanged: {
            if(x > parent.width) {
                x = 0
            } else if(x < 0) {
                x = parent.width
            }
        }

        onYChanged: {
            if(y > parent.height) {
                y = 0
            } else if(y < 0) {
                y = parent.height
            }
        }

        onFocalPointChanged: {
            if(focalPoint !== null) {
                var deltaY = focalPoint.y - (y + height / 2)
                var deltaX = focalPoint.x - (x + width / 2)
                var angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI

                rotation = angle
            }
        }

        function fireThrusters() {
            anchors.centerIn = undefined
            engine.pulse(100)
            var angle = Math.PI / 180 * rotation
            var thrustX = Math.cos(angle) * movementSpeed
            var thrustY = Math.sin(angle) * movementSpeed
            var finalTX = velocityX + thrustX
            var finalTY = velocityY + thrustY
            if(Math.abs(finalTX) + Math.abs(finalTY) < maxSpeed) {
                velocityX = finalTX
                velocityY = finalTY
            }
        }

        function fireCannon() {
            if(canFire) {
                canFire = false
                cannon.burst(1)
                cannonTimer.start()
            }
        }

        Timer {
            id: mover
            interval: 10
            repeat: true
            running: true
            onTriggered: {
                var mouseX = mouse.mouseX
                var mouseY = mouse.mouseY
                if(mouseX && mouseY)
                    ship.focalPoint = {x: mouseX, y: mouseY}
                ship.x += ship.velocityX
                ship.y += ship.velocityY
            }
        }

        Timer {
            id: cannonTimer
            interval: fireRate
            onTriggered: {
                ship.canFire = true
            }
        }

        Emitter {
            id: engine
            group: "trail"
            system: particleSystem
            width: 1; height: width
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter

            lifeSpan: 500
            lifeSpanVariation: 400
            emitRate: 500
            enabled: false

            velocityFromMovement: 1000
            velocity: AngleDirection {
                angle: ship.rotation + 180
                angleVariation: 25
                magnitude: 150
                magnitudeVariation: 50
            }
        }

        Emitter {
            id: cannon
            group: "photon"
            system: particleSystem
            width: 1; height: width
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            enabled: false

            lifeSpan: 10000
            lifeSpanVariation: 0

            velocity: AngleDirection {
                angle: ship.rotation
                angleVariation: 0
                magnitude: 150 + ship.currentSpeed()
            }
        }
    }
}
