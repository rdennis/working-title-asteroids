import QtQuick 2.0

Item {
    id: root

    width: 1; height: 1
    property alias fireTimeout: fireTimer.interval
    property var photonParent: root
    property point target
    property string collisionGroup: ""

    Item {
        id: privateVars
        property var photon: Qt.createComponent("Photon.qml", Component.Asynchronous)
    }

    function fire() {
        if(fireTimer.canFire) {
            if(privateVars.photon.status === Component.Ready) {
                var p = privateVars.photon.createObject(photonParent, {
                                                            "collisionGroup": root.collisionGroup
                                                        })

                var normalizedPoint = mapToItem(null, 0, 0)
                p.x =  normalizedPoint.x - p.width / 2
                p.y =  normalizedPoint.y - p.height / 2
                p.focalPoint = target
                p.accelerationRate = 6
                p.topSpeed = 400
                p.accelerate()
            }
            fireTimer.canFire = false
            fireTimer.start()
        }
    }

    Timer {
        id: fireTimer

        property bool canFire: true

        interval: 500
        running: false
        onTriggered: canFire = true
    }
}
