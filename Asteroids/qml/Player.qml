import QtQuick 2.0

Entity {
    id: root

    debug: false
    entityClass: "Player"

    property alias cannon: cannon

    signal killed

    accelerationRate: 3
    topSpeed: 5
    focalLock: true

    collisionGroup: "player"
    radius: width / 2

    sprite: Image {
        width: root.width
        height: root.height
        source: "../imgs/ship.png"
        smooth: true
    }

    onCollision: {
        entityVisible = false
        velocity = Qt.point(0,0)
        focalPoint = focalPoint
        accelerationRate = 0
        other.explode()
        explosion.explode()
    }


    Cannon {
        id: cannon

        x: root.width; y: root.height / 2
        photonParent: root.parent
        target: root.focalPoint
        collisionGroup: root.collisionGroup
    }

    Explosion {
        id: explosion;
        onFinished: {
            killed()
            entityVisible = true
            accelerationRate = 3
        }
    }
}
