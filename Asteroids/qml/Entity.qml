import QtQuick 2.0
import "../collisions.js" as CollisionManager

Item {
    id: root

    width: entity.width
    height: entity.height

    property Component sprite: Component {Item{}}

    property point center: Qt.point(root.x + root.width / 2, root.y + root.height / 2)
    property double radius: Math.sqrt(Math.pow(root.x - root.center.x, 2) + Math.pow(root.y - root.center.y, 2))
    property string collisionGroup: ""
    property string entityClass: "Entity"

    property alias entityRotation: entity.rotation
    property alias entityVisible: entity.visible
    property point focalPoint: Qt.point(0,0)
    property bool focalLock: false

    property double currentSpeed: Math.abs(velocity.x) + Math.abs(velocity.y)
    property double accelerationRate: 1
    property double topSpeed: 3
    property point velocity

    property bool screenWrap: true

    signal collision(var other)

    onXChanged: {
        if(screenWrap && parent) {
            var minX = -root.width / 2,
                    maxX = root.parent.width + root.width / 2
            if(x < minX)
                x = maxX
            else if(x > maxX)
                x = minX
        }
    }

    onYChanged: {
        if(screenWrap && parent) {
            var minY = -root.height / 2,
                    maxY = parent.height + root.height / 2
            if(y < minY)
                y = maxY
            else if(y > maxY)
                y = minY

        }
    }

    Component.onCompleted: {
        CollisionManager.addEntity(root)
    }

    Component.onDestruction: {
        CollisionManager.removeEntity(root)
    }

    // Rotate to face the given x,y coordinate
    function lookAt(x, y) {
        lookAtPoint(Qt.point(x,y))
    }

    // Rotate to face the given point
    function lookAtPoint(point){
        var deltaX = point.x - (x + width / 2)
        var deltaY = point.y - (y + height / 2)
        var angle = Math.atan2(deltaY, deltaX) * 180 / Math.PI
        root.rotation = angle

//        console.log("============ lookAt =============")
//        console.log("look at point: " + point.x + "," + point.y)
//        console.log("root point: " + x + "," + y)
//        console.log("entity center: " + (x + width / 2) + "," + (y + height / 2))
//        console.log("deltas: " + deltaX + "," + deltaY)
//        console.log("angle: " + angle)
    }

    // Change x and y velocity
    function accelerate() {
//        console.log("============ accelerate =============")
//        console.log("starting velocity(x,y): " + velocity.x + "," + velocity.y)

        var angle = Math.PI / 180 * root.rotation // find the direction we're facing
        var thrustX = Math.cos(angle) * accelerationRate // get thrust in x
        var thrustY = Math.sin(angle) * accelerationRate // get thrust in y
        var finalVelocity = Qt.point(velocity.x + thrustX, velocity.y + thrustY)
        // don't allow acceleration beyond max
        if(Math.floor(Math.abs(finalVelocity.x) + Math.abs(finalVelocity.y)) <= topSpeed)
            velocity = finalVelocity

//        console.log("final velocity(x,y): " + finalVelocity.x + "," + finalVelocity.y)
//        console.log("moving? " + mover.running)
    }

    onFocalPointChanged: lookAtPoint(focalPoint)

    // Moves the Entity every interval ms
    Timer {
        id: mover
        interval: 10
        repeat: true
        running: (velocity.x !== 0 || velocity.y !== 0)
        onTriggered: {
            root.x += velocity.x
            root.y += velocity.y
            if(root.focalLock)
                lookAtPoint(focalPoint)
        }
    }

    // The visual component
    Item {
        id: entity
        width: entityLoader.width
        height: entityLoader.height
        Loader { id: entityLoader; sourceComponent: sprite }
    }

    // DEBUG RECTANGLES
    property bool debug: false

    Rectangle {
        visible: debug
        width: 600; height: 1
        x: parent.width / 2
        y: parent.height / 2
        color: "cyan"
    }

    Rectangle {
        visible: debug
        anchors.fill: parent
        color: "transparent"
        border.color: mover.running ? "green" : "red"
    }

    Rectangle {
        visible: debug
        anchors.centerIn: parent
        width: radius * 2
        height: width
        radius: root.radius
        color: "transparent"
        border.color: "purple"
        z: 100
    }

    // END DEBUG RECTANGLES
}
