import QtQuick 2.0
import "../collisions.js" as CollisionManager

Entity {
    id: root

    function explode() {
            CollisionManager.removeEntity(root)
            rotate.stop()
            explosion.explode()
            sprite = null; sizeText.visible = false
    }

    Text {
        id: sizeText

        visible: root.debug
        anchors { bottom: root.top; left: root.left }
        renderType: Text.NativeRendering
        text: qsTr("size: " + size.toString())
        color: "black"
        font.pixelSize: 15
    }

    property int size: Math.random() * 4 + 1

    entityClass: "Asteroid"

    radius: root.width / 2
    velocity: Qt.point(Math.random() * (Math.floor(Math.random() * 2 + 1) % 2 == 0 ? 1 : -1),
                       Math.random() * (Math.floor(Math.random() * 2 + 1) % 2 == 0 ? 1 : -1))
    collisionGroup: "field"

    sprite: Image {
        width: sourceSize.width * size / 4
        height: sourceSize.height * size / 4
        source: "../imgs/asteroid.png"
    }

    onCollision: {
        if(size < 2)
            explode()
        else {
            root.parent.spawnAsteroid(--size, Qt.point(root.x, root.y))
        }
    }

    RotationAnimation {
        id: rotate

        running: true
        target: root
        properties: "rotation"
        loops: Animation.Infinite
        duration: 5000 + Math.random() * 5000
        from: 0; to: 360 * ((Math.floor((Math.random() * 2)) + 1) % 2 ? 1 : -1)
    }

    Explosion {
        id: explosion

        onFinished: {
            root.destroy()
        }
    }
}
