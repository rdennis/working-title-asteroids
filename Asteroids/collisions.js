/*
  * PROTOTYPE ONLY
  * A CollisionEngine will be written in C++ to handle collision detection.
  * Collision detection handled here.
  */
.pragma library

var entities = []

// entities with the same collision group will not collide
function addEntity(entity) {
    entities.push(entity)
}

function removeEntity(entity) {
    var index = entities.indexOf(entity)
    if(index >= 0) {
        entities.splice(index, 1)
    }
}

function getEntitiesInGroup(groupName) {
    var matchedEntities = []
    for (var entity in entities) {
        entity = entities[entity]
        if (entity.collisionGroup === groupName)
            matchedEntities.push(entity)
    }
    return matchedEntities
}

function getEntitiesByClass(entityClass) {
    var matchedEntities = []
    for (var entity in entities) {
        entity = entities[entity]
        if (entity.entityClass === entityClass)
            matchedEntities.push(entity)
    }
    return matchedEntities
}

function getAsteroidCount() {
    var count = 0
    var asteroids = getEntitiesByClass("Asteroid")
    for (var i in asteroids) {
        var asteroid = asteroids[i]
        count += Math.pow(2, asteroid.size - 1)
    }

    return count
}

function printEntities() {
    console.log("========== printEntities ==========")
    for(var i = 1; i < entities.length; ++i) {
        console.log(i + ":" + entities[i].collisionGroup + '(' + Math.round(entities[i].x) + "," + Math.round(entities[i].y) + ')')
    }
}

function collideEntities() {
    for(var i = 0; i < entities.length - 1; ++i) {
        for(var j = i + 1; j < entities.length; ++j) {
            var e1 = entities[i], e2 = entities[j]
            if(e1.collisionGroup !== e2.collisionGroup) {
                if(collide(e1, e2)) {
                    // Always perform Photon collision first
                    if(e2.entityClass === "Photon") { var t = e1; e1 = e2; e2 = t }
                    e1.collision(e2)
                    e2.collision(e1)
                }
            }
        }
    }
}

function collide(e1, e2) {
    var c1 = e1.center, c2 = e2.center,
            r1 = e1.radius, r2 = e2.radius,
            dist = Math.sqrt(Math.pow(c1.x - c2.x, 2) + Math.pow(c1.y - c2.y, 2))

    return dist <= r1 + r2
}
